# Changelog

## 19.12.2020

- Big refacator, only 2 packages left `app` and `extension` to make extension's development more user-friendly

## 27.09.2020

- Added `.vscode/extensions.json`
- Added `makeRequest` and `makeAuthRequest` to `ApiClient`
- Added localMigration prototype
- Added `runtime` property to `Browser.ts`
- Added `conf.ts` to `extension/`
- Added peryl dropwdon component to `ui/`
- Added log4js logger
- Updated README with **features** and **migration** sections
- Updated all dependencies
- Updated several `tsconfig.json` to support **ES2018**
- Moved `overview.drawio` to `docs/overview.drawio`
