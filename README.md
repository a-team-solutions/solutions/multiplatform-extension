# Multiplatform browser extension template

![overview](./docs/overview.png)

## Goals

Provide web extension template for scalable solutions

- Cross-browser integration
- Extendable to web platform
- Local migration example
- Minimal tech stack 

## Good Practices

**Which browsers are you going to support ?**

If you are developing web and extension, consider which browser are you going to
support. For example, when you want to support older browser for web platform, but
newer for extension platform, try to avoid using latest standards like indexedb etc.
or make different module for different platform e.g. implementing `Storage` as localstorage
for web and `Storage` as indexedb for extension.

**If possible, develop extensions as web first**

Developing web is much more faster then developing extension, because you don't
need to worry about loading and realoading your extensino in browser. Then always
test extension to make sure everything working as expected.

**Prepare for distrbuted deployment**

You can control when you deploy your website, but you **cannot** control, when your
extension will be deployed in specific stores (applestore, google play, firefox).
Sometimes you will be stucked in review process of store. That means your app will
going to use users with several platform, but different version of API. In cases
like that, is good practice to let server decide, when your apps are going to switch
to new version once your app is deployed on all platforms. In that case, logout
user from other devices if necessery.

## Packages

![packages](./docs/packages.png)