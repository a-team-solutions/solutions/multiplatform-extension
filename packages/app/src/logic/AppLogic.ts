import { UserLogic } from "./UserLogic";

export class AppLogic {

    private _userLogic: UserLogic;

    constructor(userLogic: UserLogic) {
        this._userLogic = userLogic;
    }

    user() {
        return this._userLogic;
    }

}
