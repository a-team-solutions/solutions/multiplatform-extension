import { IKVStorage } from "../interfaces/KeyValueStorage";

export class WebStorage implements IKVStorage {

    async setItem(key: string, item: string): Promise<void> {
        localStorage.setItem(key, item);
    }

    async getItem(key: string): Promise<unknown> {
        return localStorage.getItem(key);
    }

    async removeItem(key: string): Promise<void> {
        localStorage.removeItem(key);
    }

}
