/**
 * KeyValue Storage
 */
export interface IKVStorage {
    setItem(key: string, item: string): Promise<void>;
    getItem(key: string): Promise<unknown>;
    removeItem(key: string): Promise<void>;
}
