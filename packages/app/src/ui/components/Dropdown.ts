import { h } from "peryl/dist/hsml-h";

interface Props {
    items: Array<{
        value: string;
        text: string;
    }>;
}

export function dropdown(props: Props) {
    return h("div", props.items.map((item) =>
        h("option", { value: item.value }, item.text)
    ));
}
