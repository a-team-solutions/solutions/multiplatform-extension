/**
 * This script has full access to DOM on page, you can even listen on native DOM events
 * On other hand, it NO access to chrome.runtime. It can only communicate with Content script
 * using YOUR_CUSTOM_EVENT. To pass information to background, use Content script as middleman
 */

setTimeout(function () {
  // on script ready
  document.dispatchEvent(
    new CustomEvent("YOUR_CUSTOM_EVENT", {}) // payload
  );
}, 0);