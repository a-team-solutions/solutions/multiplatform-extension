// Injecting custom script to Page's context

import { injectCustomScriptToPage } from "./InjectScriptToPage";

const injectableUrl = chrome.extension.getURL("./js/inject.js");
injectCustomScriptToPage(injectableUrl);

// Make sure that "YOUR_CUSTOM_EVENT" si equal to event from your injected script
document.addEventListener("YOUR_CUSTOM_EVENT", () => {
    // console.log("[CONTENT] Msg from injected script");
});