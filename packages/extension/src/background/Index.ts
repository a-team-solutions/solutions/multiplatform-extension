import { localMigration } from "./LocalMigration";
import { WebStorage } from "../../../app/src/modules/WebStorage";
import { AppLogic } from "../../../app/src/logic/AppLogic";
import { UserLogic } from "../../../app/src/logic/UserLogic";
import { Log } from "../utils/Log";

const browser = new Browser(window);

// Log.info("conf: %s", JSON.stringify(confDefault, null, 4));

/**
 * @url {https://stackoverflow.com/questions/2399389/detect-chrome-extension-first-run-update}
 */
browser.runtime.onInstalled.addListener((details) => {
   const currentVersion = chrome.runtime.getManifest().version;
   const previousVersion = details.previousVersion;
   const reason = details.reason;

   Log.info(`Previous Version: ${previousVersion}`);
   Log.info(`Current Version: ${currentVersion}`);

   switch (reason) {
      case 'install':
         Log.info('New User installed the extension');
         break;
      case 'update':
         localMigration(previousVersion!, currentVersion); // In "update" event previousVersion must be defined
         Log.info(`Extension updated from version ${previousVersion} to ${currentVersion}`);
         break;
      case 'chrome_update':
         Log.debug('Browser updated');
      case 'shared_module_update':
         Log.debug('Shared module updated');
      default:
         Log.debug('Other install events within the browser');
         break;
   }

});

browser.runtime.onUpdateAvailable.addListener((details: any) => {
   Log.debug(`New version is available: ${details.version}`);
});

// URL with max 255 characters
// browser.runtime.setUninstallURL(url);

const webStorage = new WebStorage();

const userLogic = new UserLogic(webStorage);
const coreLogic = new AppLogic(userLogic);
