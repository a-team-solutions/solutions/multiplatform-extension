import { Log } from "../utils/Log";

export function localMigration(previousVersion: string, currentVersion: string) {
    if (previousVersion != currentVersion) {
        Log.info(`Migration from ${previousVersion} to ${currentVersion} started`);

        let version = previousVersion;
        if (version === "1.0") {
            Log.info(`Migrating to 1.1.`);
            // Migration script
            version = "1.1";
        }
        if (version === "1.1") {
            Log.info(`Migrating to 1.2`);
            // Migration script
            version = "1.2";
        }
        if (version === "1.2") {
            Log.info(`Migrating to 1.3`);
            // Migration script
            version = "1.3";
        }

        Log.info(`Migration successfully finished: ${currentVersion}`);
    }
}
