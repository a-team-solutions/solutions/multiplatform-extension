/**
 * Browser polyfill
 */
class Browser {

    public runtime: typeof chrome.runtime;

    _isSafari: boolean;
    _isChrome: boolean;
    _isAndroidFirefox: boolean;

    constructor(window: Window) {
        this._isSafari = (window["safariAppExtension"] === true);
        this._isChrome = !this._isSafari && !!window.chrome;
        this._isAndroidFirefox = (window.navigator.userAgent.indexOf('Firefox/') !== -1)
            && (window.navigator.userAgent.indexOf('Android') !== -1);
        this.runtime = window.chrome.runtime; // Check safari browser
    }

    isSafari() {
        return this._isSafari
    }

    isChrome() {
        return this._isChrome;
    }

    isAndroidFirefox() {
        return this._isAndroidFirefox;
    }

    async getCurrentActiveTab(): Promise<chrome.tabs.Tab | null> {
        return new Promise((resolve, _) => {
            chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
                if (tabs && tabs.length > 0) {
                    resolve(tabs[0]);
                }
                resolve(null);
            });
        });
    }

    async openNewTab(url?: string): Promise<chrome.tabs.Tab> {
        return new Promise((resolve, reject) => {
            chrome.tabs.create({
                url: url
            }, (tab) => {
                resolve(tab);
            });
        })
    }

    /**
     * Return current active window (not a tab!)
     */
    async getCurrentActiveWindow(): Promise<chrome.windows.Window> {
        return new Promise((resolve, reject) => {
            try {
                chrome.windows.getCurrent(window => {
                    resolve(window);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    async setBadge(tabId: number, badge: { backgroundClr?: string; text?: string; iconPath?: string }): Promise<void> {
        try {
            if (badge.iconPath) {
                chrome.browserAction.setIcon({
                    tabId: tabId,
                    path: badge.iconPath
                });
            }
            if (badge.text) {
                chrome.browserAction.setBadgeText({
                    tabId: tabId,
                    text: badge.text
                });
            }
            if (badge.backgroundClr) {
                chrome.browserAction.setBadgeBackgroundColor({
                    color: badge.backgroundClr
                });
            }
        } catch (e) {
            throw new Error(e);
        }
    };

}
