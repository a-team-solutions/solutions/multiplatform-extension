export class Log {

    static info(...msg: any) {
        console.log(msg);
    }

    static debug(...msg: any) {
        console.debug(msg);
    }

    static error(...msg: any) {
        console.error(msg);
    }

}