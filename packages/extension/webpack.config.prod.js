const CopyPlugin = require("copy-webpack-plugin");
const path = require('path');

const webpackProdConf = {
  entry: {
    background: path.resolve(__dirname, './src/background/Index.ts'),
    content: path.resolve(__dirname, './src/content/Index.ts'),
    "on-ready": path.resolve(__dirname, './src/content/injectable-scripts/on-ready.ts'),
    popup: path.resolve(__dirname, './src/popup/Index.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  devServer: {
    contentBase: __dirname + '/public'
  },
  resolve: {
    // Add '.ts' and '.tsx' as a resolvable extension.
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.json']
  },
  module: {
    rules: [
      // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'ts-loader?configFile="tsconfig.json"',
            options: {
              configFile: 'tsconfig.json'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "resources/**/*", to: "build" }
      ],
    })
  ]
};

module.exports = webpackProdConf;
